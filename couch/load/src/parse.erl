-module(parse).

%% API exports
-export([init_state/0, proc_line/2]).

-record(inp, {
  id,
  key,
  sub,
  val,
  extra = false,
  stop = false
}).

init_state() -> [].

proc_line(Line, State) ->
  [Str | _] = string:split(Line, "//", leading),
  case parse_line(string:trim(Str)) of
  #inp{} = Rec ->
    add_record(Rec, State);
  _ ->
    next
  end.

parse_line([]) ->
  [];
parse_line(Line) ->
  Opts = [{capture, all_but_first, list}],
  case re:run(Line,
    "(\\w+)(\\d+)" % Key, N
    "(_(\\w+))?"   % _, Sub
    "(\\.?)"       % End
    "\\s*"
    "("            % _
      "\"(\\w+)\"|"        % Val |
      "([\\w\\.]+)(\\\\)?" % _, Val [, \\]
    ")", Opts) of
  {match, [Key, Id, _, Sub, End, _, Val]} ->
    rec(Id, Key, Sub, Val, false, End);
  {match, [Key, Id, _, Sub, End, _, _, Val]} ->
    rec(Id, Key, Sub, Val, false, End);
  {match, [Key, Id, _, Sub, End, _, _, Val, _]} ->
    rec(Id, Key, Sub, Val, true, End);
  _Else ->
    io:format("malformed line:~n~s~n", [Line])
  end.

add_flag(#inp{extra = true}, M) -> M#{mod => true};
add_flag(_, M) -> M.

add_record(R = #inp{id = Id}, []) ->
  add_record(R, [Id | #{}]);
add_record(R = #inp{id = Id, key = Key, sub = Sub, val = Val}, [Id | A]) ->
  A1 = if Sub =:= [] ->
    add_flag(R, A#{Key => Val});
  true ->
    M = maps:get(Key, A, #{}),
    A#{Key => add_flag(R, M#{Sub => Val})}
  end,
  if R#inp.stop ->
    {ok, {Id, jsx:encode(A1)}};
  true ->
    {next, [Id | A1]}
  end;
add_record(#inp{id = Id}, [Id1 | _]) ->
  {error, {mismatch, Id, Id1}}.

rec(Id, "account", "balance", Val, Extra, End) when is_list(Val) ->
  Balance = round(list_to_float(Val) * 100),
  rec(Id, "account", "balance", Balance, Extra, End);
rec(Id, Key, Sub, Val, Extra, End) ->
  #inp{
    id = list_to_integer(Id),
    key = list_to_atom(Key),
    sub = if Sub =/= [] -> list_to_atom(Sub); true -> [] end,
    val = if is_list(Val) -> list_to_binary(Val); true -> Val end,
    extra = Extra,
    stop = End =/= []
  }.

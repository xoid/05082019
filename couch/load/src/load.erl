-module(load).

-export([main/1]).

data_url() ->
  throw({error, "make this function return correct "
    "url of the input data document"}).

couchdb_url() ->
  "http://couch:5984".

-define(DB, "/recs").
-define(TOUT, 5000).

http_get(Url) ->
  httpc:request(get, {Url, []}, http_opts(), opts()).

http_opts() ->
  [{timeout, ?TOUT}].

opts() ->
  [{body_format, binary}, {full_result, false}].

read_data(Url) ->
  case http_get(Url) of
  {ok, {200, Body}} ->
    L = parse_data(Body),
    io:format("~p~n", [L]);
  Error ->
    throw({error, Error})
  end.

parse_data(Data) ->
  proc_lines(re:split(Data, "\\R", [{return, list}])).

proc_lines([]) ->
  ok;
proc_lines(L) ->
  proc_lines(L, parse:init_state()).

proc_lines([Line | T], State) ->
  case parse:proc_line(Line, State) of
  next when T =:= [] ->
    ok;
  next ->
    proc_lines(T, State);
  {next, NewState} when T =:= [] ->
    {error, {incomplete, NewState}};
  {next, NewState} ->
    proc_lines(T, NewState);
  {ok, {N, Obj}} ->
    {Id, _Rev} = post_doc(Obj),
    io:format("record ~p id: ~p~n", [N, Id]),
    proc_lines(T);
  {error, Error} ->
    throw({error, Error})
  end.

decode_json(Bin) ->
  jsx:decode(Bin, [return_maps, {labels, atom}]).

post_doc(Obj) ->
  Url = couchdb_url() ++ ?DB,
  case httpc:request(post, {Url, [], "application/json", Obj},
    http_opts(), opts()) of
  {ok, {S, Body}} when 200 =< S; S < 300 ->
    case decode_json(Body) of
    #{ok := true, id := Id, rev := Rev} ->
      {binary_to_list(Id), binary_to_list(Rev)};
    Else ->
      throw({error, Else})
    end;
  Error ->
    throw({error, Error})
  end.

create_database() ->
  Url = couchdb_url() ++ ?DB,
  case httpc:request(put, {Url, []}, http_opts(), opts()) of
  {ok, {S, Body}} when 200 =< S; S < 300 ->
    case decode_json(Body) of
    #{ok := true} ->
      ok;
    Else ->
      throw({error, Else})
    end;
  Error ->
    throw({error, Error})
  end.

wait_for_db(N) ->
  case http_get(couchdb_url()) of
  {ok, {200, _}} ->
    ok;
  Error when N == 1 ->
    io:format("~p~n", [Error]),
    throw({error, db_connect});
  _ ->
    io:format("waiting for db to start...~n"),
    timer:sleep(1000),
    wait_for_db(N - 1)
  end.

main(_) ->
  ssl:start(),
  inets:start(),

  wait_for_db(15),
  create_database(),
  read_data(data_url()),

  ok.

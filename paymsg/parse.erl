-module(parse).

-export([
  fields/1, file/1
]).

% API function - parse message payload
%
-spec fields( string() ) ->
  [{Key::atom(), Val::string()}] | {error, Error::term()}.
fields(Str) ->
  try
    parse_field(Str, spec(), #{})
  catch error:badarg ->
    {error, {short, Str}};
  {error, Error} ->
    {error, Error}
  end.

% message fields spec: {length, type, name}
%
spec() -> [
  { 16, digits,   acct},
  {  6, digits,   pcode},
  { 12, digits,   amount},
  { 10, datetime, datetime},
  {  6, digits,   trace},
  {  6, time,     time},
  {  4, date,     date},
  {  4, date,     capdate},
  { 12, digits,   ackid},
  { 12, digits,   refnum}
].

% internal, used by fields/1
parse_field(Str, [{N, Type, Key} | T], Acc) ->
  {Head, Tail} = lists:split(N, Str),
  parse_field(Tail, T, Acc#{Key => check(Type, Head)});
parse_field([], [], Acc) ->
  maps:to_list(Acc);
parse_field([], _, Acc) ->
  throw({error, {incomplete, Acc}});
parse_field(Str, [], Acc) ->
  throw({error, {trailing_garbage, Str, Acc}}).

% internal, validate field value
%
check(digits, L) -> is_digit(L), L;
check(time, L) -> is_time(L);
check(date, L) -> is_date(L);
check(datetime, L) -> is_datetime(L).

is_digit([H | T]) when $0 =< H, H =< $9 -> is_digit(T);
is_digit([]) -> ok;
is_digit(X) -> throw({error, {bad_digit, X}}).

is_date([$0, M | T] = Date) when $0 =< M, M =< $9 ->
  case is_day(T) of true -> Date; false ->
    throw({error, {bad_date, Date}})
  end;
is_date([$1, M | T] = Date) when $0 =< M, M =< $2 ->
  case is_day(T) of true -> Date; false ->
    throw({error, {bad_date, Date}})
  end;
is_date(Date) -> throw({error, {bad_date, Date}}).

is_day([_, _] = Str) ->
  case catch list_to_integer(Str) of
  X when X > 0, X =< 31 -> true;
  _ -> false
  end.

is_time([H1,H2,M1,M2,S1,S2] = Time) when
    $0 =< H1, H1 =< $2, $0 =< H2, H2 =< $9,
    $0 =< M1, M1 =< $5, $0 =< M2, M2 =< $9,
    $0 =< S1, S1 =< $5, $0 =< S2, S2 =< $9 ->
  case catch list_to_integer([H1, H2]) of
  X when X >= 0, X =< 23 -> Time;
  _ -> throw({error, {bad_time, Time}})
  end;
is_time(Str) -> throw({error, {bad_time, Str}}).

is_datetime(Str) ->
  {Date, Time} = lists:split(4, Str),
  is_date(Date), % may throw
  is_time(Time), % may throw
  Str.

% for quick testing..
% read file with messages, print each message
% and result of parsing or error
file(Fname) ->
  {ok, File} = file:open(Fname, [read]),
  try
    proc(File)
  after
    file:close(File)
  end.

% internal, used by file/1
proc(File) ->
  case io:get_line(File, '') of
  {error, _} = Error ->
    throw(Error);
  eof ->
    ok;
  Str ->
    proc_line(Str),
    proc(File)
  end.

% internal, used by proc/1
proc_line(Line) ->
  [Str | _] = string:split(Line, "//", leading),
  parse_line(string:trim(Str)).

% internal, used by proc_line/1
parse_line([H | _] = Str) when $0 =< H, H =< $9 ->
  io:format("~p~n", [Str]),
  Ret = fields(Str),
  io:format("ret: ~p~n", [Ret]),
  ok;
parse_line(_) ->
  ok.  % ignore in this test anything unrelated

# Challenge 1
The given data looks pretty strange, It is hard to guess if there are any formatting errors/typos, or it is intentionally structured that way. I assumed second.
Also, there is no any spec on the input data format, so I had to make some initial assumptions in order to be able to implement a parsing code:

 1. Input data is plain ASCII text, not any kind of UTF.
 2. Index in a field name is only for grouping fields of the same object/record together, no need to make it part of the JSON object submitted to the CouchDB.
 3. Undescore '_' is used to denote nested object fields.
 4. Dot '.' suffix means the field is the last one in the group of fields of the same object. No other purpose.
 5. String values enclosed in double quotes may have arbitrary alphanumerics. It may be possible to extend to spaces, other symbols, even escape sequences, but I decided to keep it simple alphanums for now.
 6. Backslash at the end of balance in one record means some kind of "modifier" of the value. For example, it may negate value, or mark "pending transactions" for the balance. I don't know. May be it is just a typo. But I decided to count it as a special mark, adding boolean field "mod": true, when presented.
 7. While empty lines visually separate record blocks, I decided to ignore this fact, since there is already dot '.' mark for the end of record used.
 
The parsing code placed in separate module parse.erl, and the logic allows to parse either the list of lines or any stream of lines received one by one. The parse:proc_line/2 call returns the final result (JSON object), or continuation {next, State}, of error code.
The program implemented as escript, built using rebar3.
The program waits for CouchDB to be available, creates new DB, reads data from given S3 url, parses it and writes resulting objects to the DB.
The whole one-shot process is organized as docker-compose project.

```
macbook:couch$ docker-compose up
Creating network "couch_couchnet" with the default driver
Creating couch ... done
Creating data  ... done
Attaching to couch, data
couch      | WARNING: no logs are available with the 'none' log driver
data       | ===> Verifying dependencies...
data       | ===> Compiling load
data       | ===> Building escript...
data       | waiting for db to start...
data       | record 1 id: "35aaeb9a58779d8390f2e66478000020"
data       | record 2 id: "35aaeb9a58779d8390f2e664780004b0"
data       | record 3 id: "35aaeb9a58779d8390f2e66478000d6e"
data       | record 4 id: "35aaeb9a58779d8390f2e66478001a00"
data       | record 5 id: "35aaeb9a58779d8390f2e66478002560"
data       | ok
data exited with code 0
```
# Challenge 2
The whole content of the example file with pay messages seems to be broken, at least no messages were successfuly parsed without spec modification. I assumed, that "Data Element 32" - Acquiring institution ID code may consist of 12 characters instead of 6 as described. Only this was I was able to broke messages into sensible fields. Some reasonable validation added to field values.

The module parse.erl exposes functions to parse file with messages or single message.
```
macbook:paymsg$ erl
Erlang/OTP 21 [erts-10.3.1] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe] [dtrace]

Eshell V10.3.1  (abort with ^G)
1> c(parse).
{ok,parse}
```
parse:file/1 can be used to parse whole file printing original message and result object:
```
2> parse:file("Payment_messages_.txt").
<skipped>
```
or, parse:fields/1 can be used to parse single message:
```
3> parse:fields("0000011319353459011000000000010000080403001305102808301308040804123456123456192165102801").
<skipped>
4> 
```
